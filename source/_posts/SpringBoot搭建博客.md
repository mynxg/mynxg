---
title: SpringBoot搭建博客
# index_img: /img/myImg/abc.jpg
categories:
  - 技术栈
tags:
  - Project
  - java
  - SpringBoot
  - spring
date: 2022-06-21 23:46:17
abbrlink: 12284
---

# 准备工作

## 环境

Idea2020.1、JDK8、Maven3.3.9、Node.js、VScode、Anaconda

## 需要使用的技术

 SpringBoot、Mybatis-plus、Lombok、Mysql、Nginx

Element Plus、Vue3、Markdown编辑器

# 相关

## 需求分析

前端布局设计：首页、登录注册页面、写文章发布博客界面.....

## 数据库设计

登录、注册（包含用户名、密码、年龄等）、需要设计多张表



## 前端页面编写

一般套用框架比较快，使用Anaconda环境、element-plus 框架或其他前端框架写前端页面



## 后端接口编写

登录接口、注册接口、





## Vue框架的使用

展示数据到页面

# 参考文档

Java学习路线;https://github.com/liyupi/code-roadmap/blob/main/docs/roadmap/Java%E5%AD%A6%E4%B9%A0%E8%B7%AF%E7%BA%BF.md

Maven教程：https://juejin.cn/post/7087224245549269000

Spring Boot 官网：https://spring.io/quickstart 

Mybatis Plus 官网：https://baomidou.com/pages/24112f/ 

Lombok：官网：https://projectlombok.org/setup/mave

Vue3 官网：https://v3.cn.vuejs.org/guide/introduction.html 

Element Plus 官网：https://element-plus.org/zh-CN/guide/installation.html 

