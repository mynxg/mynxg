---
title: 友情链接 | link
date: 2020-10-31 23:38:35
type: "link"
sitemap: false
---

## "A Few Requirements

--------

## "swap friend url

* 如果你想要和我交换友链，请在下方留言或发邮件到 mynxg@qq.com 即可
* 记得备注好你的 name, url, img, description 等信息哦
  
--------

## "My Friend Link Information

```yml
name: 小倪的博客
link: https://blog.lxip.top/
avatar: https://pic.imgdb.cn/item/640d9bf1f144a010073218ac.png
descr: 用这生命中的每一秒，给自己一个不后悔的未来

# 选填：
siteshot: https://pic.imgdb.cn/item/642d63d3a682492fcc6a37a1.jpg
```